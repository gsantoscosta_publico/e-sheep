<?php include("include/cabecalho.php") ?>

<link rel="stylesheet" href="css/produtos.css">
    
    <?php if (Sessao::isUsuarioLogado()) : ?>
        <form action="/produto_editar.php" method="post">
            <button class="botao botao-novo" type="submit">novo produto</button>
        </form>
    <?php endif; ?>

    <table class="lista">
        <?php 
        
            $produtos = (new ProdutoDAO(Conexao::getConexao()))->lista($busca, 1); 
            
            if (empty($produtos)) :

        ?>
                        
                            
                <h2 class="obs">Nenhum produto encontrado :(</h2>
                            
        <?php 
        
            else:
            
                foreach($produtos as $produto) :
            
        ?>

                    <tr>
                    
                        <td class="dado img-produto"><img src="https://loremflickr.com/80/80?lock=<?= $produto->id ?>" /></div>

                        <td class="dado">

                            <h3><?= $produto->nome ?></h3>
                            <h3 class="preco">R$<?= number_format($produto->valor, 2, ",", "."); ?></h3>
                            <?= $produto->descricao ?><br/>
                            
                            <div class="tags">

                                <?php foreach($produto->familias as $familia) : ?>
                                    <div class=tag>
                                        <?= $familia ?>
                                    </div>
                                <?php endforeach; ?>

                            </div>

                        </td>

                        <?php if (Sessao::isUsuarioLogado()) : ?>
                            <td class="controle">
                                <form action="/produto_editar.php" method="post">

                                    <input type="hidden" name="id" value="<?=$cadastro->id?>">
                                    <button class="botao" type="submit">editar</button>

                                </form>
                            </td>

                            <td class="controle">
                                <form action="/produto_excluir.php" method="post">

                                    <input type="hidden" name="id" value="<?=$cadastro->id?>">
                                    <button class="botao botao-atencao" type="submit">excluir</button>

                                </form>
                            </td>
                        <?php endif; ?>
                    
                    </tr>

        <?php 
        
                endforeach;

            endif;
        ?>
    </table>
    
<?php include("include/rodape.php"); ?>
