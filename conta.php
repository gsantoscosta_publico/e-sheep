<?php 

    require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");
    
    if (! Sessao::isUsuarioLogado()) {
        header("Location: /login.php");
        die();
    }

    $usuario = Sessao::getUsuario();
?>

<?php include("include/cabecalho.php") ?>

<link rel="stylesheet" href="css/conta.css">
    
    <div class="avatar">
        <img src="https://api.adorable.io/avatars/124/e-sheep_usuario<?= $usuario->id ?>.png" alt="usuario avatar"/>
    </div>

    <div class="info">
        <h1><?= $usuario->nome ?></h1>
        <h1><?= $usuario->email ?></h1>
    </div>

    <form action="/controle/logout.php" method="post">
        <div class="formulario">
            <div class="botoes">
                <button class="botao botao-grande" type="submit">Sair</button></td>
            </div>
        </div>
    </form>    

<?php include("include/rodape.php"); ?>
