<?php 

    require_once("controle/autoload.php");

    if (Sessao::isUsuarioLogado()) {
        header("Location: /conta.php");
        die();
    }

?>

<?php include("include/cabecalho.php"); ?>

    <form action="/controle/login.php" method="post">
        <h1>Login</h1>

        <table class="formulario">
        
            <tr>
                <td class="titulos">e-mail</td>
                <td><input type="email" name="email"> <span class="obs">não sabe qual usuário usar? tente "joao@bol.com.br"</span></td>
            </tr>
            
            <tr>
                <td class="titulos">senha</td>
                <td><input type="password" name="senha"> <span class="obs">a senha do usuário João é "123456"</span></td>
            </tr>
            
            <tr>
                <td class="botoes" colspan="2"><button class="botao botao-grande" type="submit">Entrar</button></td>
            </tr>
            
        </table>
    </form>
    
<?php include("include/rodape.php"); ?>
