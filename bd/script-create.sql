CREATE DATABASE IF NOT EXISTS e-sheep
  CHARSET = UTF8 COLLATE = utf8_general_ci;
  
USE eatX;

CREATE TABLE IF NOT EXISTS usuario (
  id int auto_increment NOT NULL,
  email varchar(100) NOT NULL,
  senha varchar(255) NOT NULL,
  nome varchar(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE UNIQUE INDEX usuario_uniq_email ON usuario (email);

CREATE TABLE IF NOT EXISTS familia (
  id int auto_increment NOT NULL,
  nome varchar(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS produto (
  id int auto_increment NOT NULL,
  nome varchar(100) NOT NULL,
  descricao text,
  valor float,  
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS produtofamilia (
  id int auto_increment NOT NULL,
  id_produto int NOT NULL,
  id_familia int NOT NULL,
  FOREIGN KEY (id_produto) REFERENCES produto(id),
  FOREIGN KEY (id_familia) REFERENCES familia(id),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cadastro (
  id int auto_increment NOT NULL,
  nome varchar(100) NOT NULL,
  cpf varchar(11) NOT NULL,
  rg varchar(20),
  dataNascimento date,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cadastrotelefone (
  id int auto_increment NOT NULL,
  id_cadastro int NOT NULL,
  telefone varchar(20) NOT NULL,
  FOREIGN KEY (id_cadastro) REFERENCES cadastro(id),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cadastroemail (
  id int auto_increment NOT NULL,
  id_cadastro int NOT NULL,
  email varchar(100) NOT NULL,
  FOREIGN KEY (id_cadastro) REFERENCES cadastro(id),
  PRIMARY KEY (id)
);