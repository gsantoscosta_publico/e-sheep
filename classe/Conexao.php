<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class Conexao {

    static function getConexao() {
        $conexao = mysqli_connect("localhost", "root", "123456", "e-sheep");
        
        if (!$conexao) {
            throw new Exception("Erro ao conectar no banco de dados");

        } else {
            mysqli_set_charset($conexao, "utf8");
        }

        return $conexao;
    }
    
}