<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class Sessao {

    static function setUsuario($usuario) {
        $_SESSION["usuario"] = $usuario;
    }

    static function getUsuario() {
        return $_SESSION["usuario"];
    }

    static function isUsuarioLogado() {
        return isset($_SESSION["usuario"]);
    }

    static function setMensagemAlerta($mensagem) {
        $_SESSION["mensagem_alerta"] = $mensagem;
    }

    static function getMensagemAlerta() {
        $mensagem = $_SESSION["mensagem_alerta"];
        unset($_SESSION["mensagem_alerta"]);
        return $mensagem;
    }

    static function hasMensagemAlerta() {
        return isset($_SESSION["mensagem_alerta"]);
    }

    static function setMensagemInfo($mensagem) {
        $_SESSION["mensagem_info"] = $mensagem;
    }

    static function getMensagemInfo() {
        $mensagem = $_SESSION["mensagem_info"];
        unset($_SESSION["mensagem_info"]);
        return $mensagem;
    }

    static function hasMensagemInfo() {
        return isset($_SESSION["mensagem_info"]);
    }

}