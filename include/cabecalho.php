<?php 

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php"); 
        
    $busca = "";

    if (isset($_GET["busca"])) {
        $busca = $_GET["busca"];
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>e-Sheep</title>
        
        <link rel="icon" href="img/favicon.png"/>
        
        <script src="js/global.js"></script>
        
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/global.css">
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700">
    </head>
    
    <body>

        <div class="cabecalho">
        
            <div class="logo">
                <img src="img/logo.png" alt="e-sheep logo"/>
                <h1>e-Sheep</h1>
            </div>
           
            <form class="busca" action="produtos.php">
                <input type="text" placeholder="encontre um produto..." name="busca" value="<?= $busca ?>"/>
            </form>

            <nav class="menu">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="produtos.php">Produtos</a></li>
                    
                    <?php if (Sessao::isUsuarioLogado()) : ?>
                        <li>
                            <a href="cadastros.php">Clientes</a>
                        </li>
                    <?php endif; ?>
                    
                    <li><a href="sobre.php">Sobre</a></li>
                    <li>
                    
                        <?php if (Sessao::isUsuarioLogado()) : ?>
                            <a href="conta.php">Conta</a>
                        <?php else: ?>
                            <a href="login.php">Login</a>
                        <?php endif; ?>
                    
                    </li>
                </ul>
            </nav>
            
        </div>
    
        <main class="conteudo">
        
            <?php if (Sessao::hasMensagemAlerta()) : ?>
                <span class="mensagem-alerta">
                    <?= Sessao::getMensagemAlerta(); ?>
                </span>
            <?php endif; ?>
            
            <?php if (Sessao::hasMensagemInfo()) : ?>
                <span class="mensagem-info">
                    <?= Sessao::getMensagemInfo(); ?>
                </span>
            <?php endif; ?>