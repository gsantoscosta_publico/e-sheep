<?php 

    require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");
    
    if (!Sessao::isUsuarioLogado()) {
        header("Location: /login.php");
        die();
    }
    
    $cadastro = new CadastroVO();
    
    if (isset($_GET['id'])) {
        $cadastro = (new CadastroDAO(Conexao::getConexao()))->get($_GET["id"]);
    }

?>

<?php include("include/cabecalho.php") ?>

<link rel="stylesheet" href="css/cadastro_editar.css">

    <form action="/controle/cadastro.php" method="post">
        <?php if ($cadastro->isNovo()) : ?>
            <h1>Novo Cadastro</h1>
        <?php else: ?>
            <h1>Editar Cadastro</h1>
        <?php endif; ?>
        
        <input type="hidden" name="id" value="<?=$cadastro->id?>">

        <table class="formulario">
                
            <tr>
                <td class="titulos">nome</td>
                <td><input name="nome" size="50" maxlength="100" required value="<?= $cadastro->nome ?>"></td>
            </tr>
            
            <tr>
                <td class="titulos">cpf</td>
                <td><input name="cpf" type="number" size="11" maxlength="11" required value="<?= $cadastro->cpf ?>"> <span class="obs">apenas números</span></td>
            </tr>
            
            <tr>
                <td class="titulos">rg</td>
                <td><input name="rg" type="number" size="11" maxlength="20" value="<?= $cadastro->rg ?>"> <span class="obs">apenas números</span></td>
            </tr>
            
            <tr>
                <td class="titulos">nascimento</td>
                <td><input type="date" name="dataNascimento" value="<?= $cadastro->dataNascimento ?>"></td>
            </tr>
            
            <tr>
                <td class="titulos">telefone</td>

                <td>
                    <div class="multiplos-valores">
                        <?php foreach($cadastro->telefones as $fone) :?>
                            <div class="valor">
                                <input name="telefones[]" type="number" size="11" maxlength="20" value="<?= $fone ?>">
                                <a title="remover telefone" onclick="removerDeMultiplosValores(this);"><img src="img/remove.png" alt="remover"/></a>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="multiplos-valores-controle">
                        <a href="" title="adicionar telefone"><img src="img/add.png" alt="adicionar"/></a>
                        <span class="obs">apenas números</span>
                    <div>
                
                </td>
            </tr>

            <tr>
                <td class="titulos">e-mail</td>

                <td>
                    <div class="multiplos-valores">
                        <?php foreach($cadastro->emails as $email) :?>
                            <div class="valor">
                                <input name="emails[]" type="email" size="50" maxlength="100" value="<?= $email ?>">
                                <a title="remover email" onclick="removerDeMultiplosValores(this);"><img src="img/remove.png" alt="remover"/></a>
                            </div>
                        <?php endforeach; ?>

                        <div class="valor">
                            <input name="emails[]" type="email" size="50" maxlength="100">
                            <a title="adicionar email" onclick="adicionarDeMultiplosValores(this);"><img src="img/add.png" alt="adicionar"/></a>                        
                        <div>
                    </div>
                
                </td>
            </tr>
            
            <tr>
                <td class="botoes" colspan="2"><button class="botao botao-grande" type="submit">Salvar</button></td>
            </tr>      
            
        </table>
    </form>
    
<?php include("include/rodape.php"); ?>
