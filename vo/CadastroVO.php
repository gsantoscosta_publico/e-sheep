<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class CadastroVO {
    
    public $id = 0;
    public $nome = "";
    public $dataNascimento = "";
    public $cpf = "";
    public $rg = "";
    public $telefones = array();
    public $emails = array();
    
    function isNovo() {
        return $this->id <= 0;
    }
    
}