<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class ProdutoVO {
    
    public $id = 0;
    public $nome = "";
    public $descricao = "";
    public $valor = 0.0;
    public $familias = array();
    
}