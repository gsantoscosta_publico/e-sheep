<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class CadastroDAO extends AcessoBaseDAO {

    const LIMITE_POR_PAGINA = 5;

    function lista(String $busca, int $pagina) {

        $cadastros = array();

        if ($pagina <= 0) {
            $pagina = 1;
        }

        $sql = $this->getSQLLista($busca);
        
        /*$sql = $sql . " LIMIT " . $this::LIMITE_POR_PAGINA;
        
        $sql = $sql . " OFFSET " . (($pagina - 1) * $this::LIMITE_POR_PAGINA);*/

        $query = $this->getQuery($sql);

        while ($linha = $this->getRow()) {
            array_push($cadastros, $this->parse($linha));
        }
            
        return $cadastros;
    }
    
    private function getSQLLista(String $busca, bool $isCount = false) {

        $busca = $this->preparaTextoParaSQL($busca);
    
        $sql = "SELECT";
        
        if ($isCount) {
            $sql = $sql . " COUNT(1)";
            
            $sql = $sql . " FROM cadastro AS c";

        } else {
            $sql = $sql . " c.*, ";
            $sql = $sql . " GROUP_CONCAT(DISTINCT REPLACE(ct.telefone, ';', ',') SEPARATOR ';') AS telefones,";
            $sql = $sql . " GROUP_CONCAT(DISTINCT REPLACE(ce.email, ';', ',') SEPARATOR ';') AS emails";
            
            $sql = $sql . " FROM cadastro AS c";
            
            $sql = $sql . " LEFT JOIN cadastrotelefone AS ct ON ct.id_cadastro = c.id";
            $sql = $sql . " LEFT JOIN cadastroemail AS ce ON ce.id_cadastro = c.id";
        }
        
        if ($busca != "") {
            $sql = $sql . " WHERE (LOWER(c.nome) LIKE LOWER('%{$busca}%')) OR ";
            $sql = $sql . " (c.cpf = '%{$busca}%') OR ";
            $sql = $sql . " (c.rg = '%{$busca}%')";
        }        
        
        $sql = $sql . " GROUP BY c.id";
        
        $sql = $sql . " ORDER BY c.nome";
        
        return $sql;
    }
    
    function get(int $id) {

        $sql = "SELECT";
        $sql = $sql . " c.*, ";
        $sql = $sql . " GROUP_CONCAT(DISTINCT REPLACE(ct.telefone, ';', ',') SEPARATOR ';') AS telefones,";
        $sql = $sql . " GROUP_CONCAT(DISTINCT REPLACE(ce.email, ';', ',') SEPARATOR ';') AS emails";
        
        $sql = $sql . " FROM cadastro AS c";
        
        $sql = $sql . " LEFT JOIN cadastrotelefone AS ct ON ct.id_cadastro = c.id";
        $sql = $sql . " LEFT JOIN cadastroemail AS ce ON ce.id_cadastro = c.id";
        
        $sql = $sql . " WHERE (c.id = '{$id}')";
        
        $sql = $sql . " GROUP BY c.id";
        
        $query = $this->getQuery($sql);

        $linha = $this->getRow();
        $cadastro = $this->parse($linha);
            
        return $cadastro;
    }
    
    private function parse(array $fetch) {

        if ($fetch == null) {
            return null;
        }

        $cadastro = new CadastroVO();
        
        $cadastro->id = $fetch["id"];
        $cadastro->nome = $fetch["nome"];
        $cadastro->dataNascimento = $fetch["dataNascimento"];
        $cadastro->cpf = $fetch["cpf"];
        $cadastro->rg = $fetch["rg"];

        if ($fetch["telefones"] != '') {
            $cadastro->telefones = explode(";", $fetch["telefones"]);
        }

        if ($fetch["emails"] != '') {
            $cadastro->emails = explode(";", $fetch["emails"]);
        }
        
        return $cadastro;
    }
    
    function salvar(CadastroVO $cadastro) {
        
        $sql = "";
        
        $dataNascimento = (!isset($cadastro->dataNascimento) || trim($cadastro->dataNascimento) === '') ? "null" : "'{$this->preparaTextoParaSQL($cadastro->dataNascimento)}'";
        
        if ($cadastro->isNovo()) {
            $sql = "INSERT INTO CADASTRO (nome, cpf, rg, dataNascimento) VALUES (";

            $sql = $sql . "'{$this->preparaTextoParaSQL($cadastro->nome)}', ";
            $sql = $sql . "'{$this->preparaTextoParaSQL($cadastro->cpf)}', ";
            $sql = $sql . "'{$this->preparaTextoParaSQL($cadastro->rg)}', ";
            $sql = $sql . $dataNascimento;

            $sql = $sql . ");";

        } else {
            $sql = "UPDATE CADASTRO SET";

            $sql = $sql . " nome = '{$this->preparaTextoParaSQL($cadastro->nome)}',";
            $sql = $sql . " cpf = '{$this->preparaTextoParaSQL($cadastro->cpf)}',";
            $sql = $sql . " rg = '{$this->preparaTextoParaSQL($cadastro->rg)}',";
            $sql = $sql . " dataNascimento = {$dataNascimento}";

            $sql = $sql . " WHERE ";
            $sql = $sql . " id = {$cadastro->id};";
        }

        $this->begin();
        try {
            $this->executar($sql);

            $this->commit();

        } catch(Exception $e) {
            $this->rollback();
            error_log($e);
            throw $e;
        }
    }
        
    function excluir(int $id) {

        $sql = "";
        
        $sql = "DELETE FROM cadastrotelefones WHERE id = {$id};";
        $sql = "DELETE FROM cadastroemails WHERE id = {$id};";
        $sql = "DELETE FROM cadastro WHERE id = {$id};";

        $this->begin();
        try {
            $this->executar($sql);

            $this->commit();

        } catch(Exception $e) {
            $this->rollback();
            error_log($e);
            throw $e;
        }
    }
}