<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class ProdutoDAO extends AcessoBaseDAO {

    const LIMITE_POR_PAGINA = 5;

    function lista(String $busca, int $pagina) {

        $produtos = array();

        if ($pagina <= 0) {
            $pagina = 1;
        }

        $sql = $this->getSQLLista($busca);
        
        /*$sql = $sql . " LIMIT " . $this::LIMITE_POR_PAGINA;
        
        $sql = $sql . " OFFSET " . (($pagina - 1) * $this::LIMITE_POR_PAGINA);*/

        $query = $this->getQuery($sql);

        while ($linha = $this->getRow()) {
            array_push($produtos, $this->parse($linha));
        }

        return $produtos;
    }
    
    private function getSQLLista(String $busca, bool $isCount = false) {

        $busca = $this->preparaTextoParaSQL($busca);
    
        $sql = "SELECT";
        
        if ($isCount) {
            $sql = $sql . " COUNT(1)";
            
            $sql = $sql . " FROM produto AS p";

        } else {
            $sql = $sql . " p.*, GROUP_CONCAT(DISTINCT REPLACE(f.nome, ';', ',') SEPARATOR ';') AS familias";
            
            $sql = $sql . " FROM produto AS p";
            
            $sql = $sql . " LEFT JOIN produtofamilia AS pf ON pf.id_produto = p.id";
            $sql = $sql . " LEFT JOIN familia AS f ON f.id = pf.id_familia";
        }
        
        if ($busca != "") {
            $sql = $sql . " WHERE LOWER(p.nome) LIKE LOWER('%{$busca}%')";
        }        
        
        $sql = $sql . " GROUP BY p.id";
        
        $sql = $sql . " ORDER BY p.nome";
        
        return $sql;
    }
    
    private function parse(array $fetch) {

        if ($fetch == null) {
            return null;
        }

        $produto = new ProdutoVO();
        
        $produto->id = $fetch["id"];
        $produto->nome = $fetch["nome"];
        $produto->descricao = $fetch["descricao"];
        $produto->valor = $fetch["valor"];
        
        if ($fetch["familias"] != '') {
            $produto->familias = explode(";", $fetch["familias"]);
        }
        
        return $produto;
    }
    
}