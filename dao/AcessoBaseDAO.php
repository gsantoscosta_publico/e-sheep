<?php 

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class AcessoBaseDAO {

    private $conexao;
    protected $query;

    function __construct($conexao) {
        $this->setConexao($conexao);
    }

    function setConexao($conexao) {
        $this->conexao = $conexao;
    }

    function getConexao() {
        if (!$this->conexao) {
            throw new Exception('Não conectado ao banco de dados');
        }
        
        return $this->conexao;
    }
    
    function preparaTextoParaSQL(String $texto) {
        return mysqli_real_escape_string($this->getConexao(), $texto);
    }

    function getQuery (String $sql = null) {
        if ($sql != null) {
            $this->query = mysqli_query($this->getConexao(), $sql);
            
            if (!$this->query) {
                error_log("erro ao executar: " . $sql);
                throw new Exception($this->getConexao()->error);
            }
        }

        return $this->query;
    }
    
    function getRow(String $sql = null) {
        return mysqli_fetch_assoc($this->getQuery($sql));
    }
    
    function executar(String $sql) {
        if (!mysqli_query($this->getConexao(), $sql)) {
            error_log("erro ao executar: " . $sql);
            throw new Exception($this->getConexao()->error);
        }
    }
    
    function begin() {
        $this->getConexao()->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
    }
    
    function commit() {
        $this->getConexao()->commit();
    }
    
    function rollback() {
        $this->getConexao()->rollback();
    }

}