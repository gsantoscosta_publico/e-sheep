<?php 

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

class UsuarioDAO extends AcessoBaseDAO {

    function get(String $email, String $senha) {
        $email = $this->preparaTextoParaSQL($email);
        $senha = $this->preparaTextoParaSQL($senha);

        $sql = "select * from usuario where email='{$email}' and senha=md5('{$senha}')";
        
        return $this->parse($this->getRow($sql));
    }
    
    private function parse($fetch) {

        if ($fetch == null) {
            return null;
        }

        $usuario = new UsuarioVO();
        
        $usuario->id = $fetch["id"];
        $usuario->nome = $fetch["nome"];
        $usuario->email = $fetch["email"];
        
        return $usuario;
    }
    
}