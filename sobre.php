<?php include("include/cabecalho.php") ?>

    <h1>Sobre a Página</h1>

    <p>e-Sheep é uma página construída como parte de um processo de avaliação.</p>

    <p>Seguindo algumas especificações, ela foi construída em PHP, com acesso à dados em um banco MySQL.</p>

    <p>O leiaute todo criado com HTML5 e CSS puro, sem uso de bibliotecas.</p>

    <p>O código da página está disponível no GitLab e pode ser encontrado em <a href="https://gitlab.com/gsantoscosta_publico/e-sheep">https://gitlab.com/gsantoscosta_publico/e-sheep</a>

    <h1>Próximos Passos</h1>

    <p>Há melhorias a serem feitas na página <strike>(que não dariam tempo de fazer, rsrs)</strike>, como por exemplo:</p>

    <ul>
        <li>Validar e formatar melhor os dados nos formulários e listas.</li>
        <li>Verificar e adaptar o layout das páginas para outros navegadores ou dispositivos, foi homologado apenas no Google Chrome em desktop.</li>
        <li>Paginar as listas para evitar grandes esperas ou sobrecargas no navegador.</li>
        <li>Redirecionar o usuário à uma página de erro quando algo inesperado acontecer, evitando a má impressão com o site.</li>
        <li>Mesmo em PHP, poderiam ser criados web-services respondendo requisições REST, utilizando JavaScript para a comunicação com esses serviços, evitando a interação direta dos códigos PHP nas páginas HTML.</li>
    </ul>
    
    <h1>Sobre Mim</h1>

    <p>Desenvolvedor de Software Sênior de Limeira/SP</p>

    <p>Fabricando soluções há mais de 10 anos, com maior atuação em softwares de gestão,</p>

    <p>zelando sempre pela boa qualidade, simplicidade, integridade e escalabilidade do código fonte.</p>

    <p>Mais informações sobre experiência e contato em <a href="https://www.linkedin.com/in/gsantoscosta/">https://www.linkedin.com/in/gsantoscosta/</a>

<?php include("include/rodape.php"); ?>