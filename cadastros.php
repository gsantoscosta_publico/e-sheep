<?php 

    require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");
    
    if (! Sessao::isUsuarioLogado()) {
        header("Location: /login.php");
        die();
    }

?>

<?php include("include/cabecalho.php") ?>
    
    <form action="/cadastro_editar.php" method="post">
        <button class="botao botao-novo" type="submit">novo cadastro</button>
    </form>

    <table class="lista">

        <?php 
        
            $cadastros = (new CadastroDAO(Conexao::getConexao()))->lista($busca, 1); 
            
            if (empty($cadastros)) :

        ?>
                        
                            
                <h2 class="obs">Nenhum cadadastro encontrado :(</h2>
                            
        <?php 
        
            else:
            
                foreach($cadastros as $cadastro) :
            
        ?>

                    <tr>
                    
                        <td class="dado">

                            <h3><?= $cadastro->nome ?></h3>
                            
                            <div class="tags">
                            
                                <img src="img/fone.png" alt="fone"/>

                                <?php foreach($cadastro->telefones as $telefone) : ?>
                                    <div class=tag>
                                        <?= $telefone ?>
                                    </div>
                                <?php endforeach; ?>

                            </div>

                        </td>
         
                        <td class="controle">
                            <form action="/cadastro_editar.php" method="get">

                                <input type="hidden" name="id" value="<?=$cadastro->id?>">
                                <button class="botao" type="submit">editar</button>

                            </form>
                        </td>

                        <td class="controle">
                            <form action="/controle/cadastro_excluir.php" method="post">

                                <input type="hidden" name="id" value="<?=$cadastro->id?>">
                                <button class="botao botao-atencao" type="submit">excluir</button>

                            </form>
                        </td>

                    </tr>

        <?php 
        
                endforeach;

            endif;
        ?>
    </table>
    
<?php include("include/rodape.php"); ?>
