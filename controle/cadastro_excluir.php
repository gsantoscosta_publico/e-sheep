<?php 

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

if ((!isset($_POST["id"]))) {
    Sessao::setMensagemAlerta("parametros incorretos.");
    header("Location: /cadastros.php");
}

try {
    (new CadastroDAO(Conexao::getConexao()))->excluir($_POST["id"]);
    Sessao::setMensagemInfo("excluido com sucesso.");

} catch (Exception $e) {
    Sessao::setMensagemAlerta("Ops, um erro ocorreu ao excluir.");
}
    
header("Location: /cadastros.php");
die();