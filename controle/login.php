<?php 

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

if ((!isset($_POST["email"])) || (!isset($_POST["senha"]))) {
    Sessao::setMensagemAlerta("email ou senha inválido.");
    header("Location: /login.php");
}

$usuario = (new UsuarioDAO(Conexao::getConexao()))->get($_POST["email"], $_POST["senha"]);

if ($usuario == null) {
    Sessao::setMensagemAlerta("email ou senha inválido.");
    header("Location: /login.php");

} else {
    Sessao::setUsuario($usuario);
    header("Location: /index.php");
}

die();