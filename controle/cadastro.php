<?php 

require_once($_SERVER["DOCUMENT_ROOT"]."/controle/autoload.php");

$cadastro = new CadastroVO();

if (!isset($_POST['id'])) {
    throw new Exception("id não informado");
}

$cadastro->id = $_POST['id'];

if (isset($_POST['nome'])) {
    $cadastro->nome = $_POST['nome'];
}

if (isset($_POST['dataNascimento'])) {
    $cadastro->dataNascimento = $_POST['dataNascimento'];
}

if (isset($_POST['cpf'])) {
    $cadastro->cpf = $_POST['cpf'];
}

if (isset($_POST['rg'])) {
    $cadastro->rg = $_POST['rg'];
}

if (isset($_POST['telefones'])) {
    $cadastro->telefones = $_POST['telefones'];
}

if (isset($_POST['emails'])) {
    $cadastro->emails = $_POST['emails'];
}

try {
    (new CadastroDAO(Conexao::getConexao()))->salvar($cadastro);
    Sessao::setMensagemInfo("salvo com sucesso.");

} catch (Exception $e) {
    Sessao::setMensagemAlerta("Ops, um erro ocorreu ao salvar.");
}
    
header("Location: /cadastros.php");
die();