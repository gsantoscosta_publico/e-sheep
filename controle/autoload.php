<?php

class AutoLoader {

    private $diretorio;

    public function __construct($diretorio) {
        $this->diretorio = $diretorio;
    }

    public function autoload($class_name) {
        $arquivo = $_SERVER["DOCUMENT_ROOT"] . "/" . $this->diretorio . "/" . $class_name . ".php";

        if (file_exists($arquivo) == false) {
            return false;
        }

        require_once($arquivo);
    }
    
}

$controle_loader = new autoloader("controle");
$dao_loader = new autoloader("dao");
$classe_loader = new autoloader("classe");
$vo_loader = new autoloader("vo");

spl_autoload_register(array($controle_loader, "autoload"));
spl_autoload_register(array($dao_loader, "autoload"));
spl_autoload_register(array($classe_loader, "autoload"));
spl_autoload_register(array($vo_loader, "autoload"));